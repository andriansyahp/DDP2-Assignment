/**
* Class JavariPark merupakan Main Class.
* berfungsi meng-handle input dan memberikan output pada user
*/

import animal.Animal;
import animal.Cat;
import animal.Eagle;
import animal.Hamster;
import animal.Lion;
import animal.Parrot;
import cage.ArrangeCage;
import cage.Cage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class JavariPark {
    static Cat catInit;
    static Eagle eagleInit;
    static Hamster hamsterInit;
    static Lion lionInit;
    static Parrot parrotInit;
    static Cage cageInit;
    static ArrayList<Cage>[] cageArray = (ArrayList<Cage>[]) new ArrayList[5];
    static ArrayList<Cage> catCageList = new ArrayList<Cage>();
    static ArrayList<Cage> hamsterCageList = new ArrayList<Cage>();
    static ArrayList<Cage> eagleCageList = new ArrayList<Cage>();
    static ArrayList<Cage> parrotCageList = new ArrayList<Cage>();
    static ArrayList<Cage> lionCageList = new ArrayList<Cage>();
    static String[] availableAnimals = {"cat", "lion", "eagle", "parrot", "hamster"};

    // Membuat main method yang memanggil tiap fungsi sesuai urutan
    public static void main(String[] args) {
        animalInstantiation();
        System.out.println("\n=============================================");
        cageArrangement();
        System.out.println("\n=============================================");
        zooVisit();
    }

    // Membuat method tahap pertama, yaitu penginstansiasian objek hewan serta kandangnya
    public static void animalInstantiation() {
        Scanner input = new Scanner(System.in);

        System.out.println("Welcome to Javari Park!\nInput the number of the animals");
        for (String animal : availableAnimals) {
            System.out.printf("%s: ", animal);
            int numOfAnimals = Integer.parseInt(input.nextLine());

            if (numOfAnimals > 0) {
                System.out.printf("Provide the information of %s(s):\n", animal);
                String[] list1D = input.nextLine().split(",");
                String[][] list2D = new String[list1D.length][2];

                for (int i = 0; i < list1D.length; i++) {
                    list2D[i] = list1D[i].split("\\|");

                    if (animal.equals("cat")) {
                        catInit = new Cat(list2D[i][0], Integer.parseInt(list2D[i][1]));
                        cageInit = new Cage(catInit);
                        catCageList.add(cageInit);

                    } else if (animal.equals("eagle")) {
                        eagleInit = new Eagle(list2D[i][0], Integer.parseInt(list2D[i][1]));
                        cageInit = new Cage(eagleInit);
                        eagleCageList.add(cageInit);

                    } else if (animal.equals("hamster")) {
                        hamsterInit = new Hamster(list2D[i][0], Integer.parseInt(list2D[i][1]));
                        cageInit = new Cage(hamsterInit);
                        hamsterCageList.add(cageInit);

                    } else if (animal.equals("lion")) {
                        lionInit = new Lion(list2D[i][0], Integer.parseInt(list2D[i][1]));
                        cageInit = new Cage(lionInit);
                        lionCageList.add(cageInit);

                    } else {
                        parrotInit = new Parrot(list2D[i][0], Integer.parseInt(list2D[i][1]));
                        cageInit = new Cage(parrotInit);
                        parrotCageList.add(cageInit);
                    }
                }
            }
        }

        // Memasukkan tiap list yang berisi kandang ke dalam array
        // yang berfungsi sebagai penampung kandang yang terbagi berdasarkan tipe hewan
        cageArray[0] = catCageList;
        cageArray[1] = lionCageList;
        cageArray[2] = parrotCageList;
        cageArray[3] = eagleCageList;
        cageArray[4] = hamsterCageList;

        System.out.println("Animals have been successfully recorded!");
    }

    // Membuat method untuk melakukan tahap kedua, yaitu memberikan informasi kandang
    // yang diatur melalui pengaturan
    public static void cageArrangement() {
        System.out.print("Cage arrangement:");
        for (int i = 0; i < cageArray.length; i++) {
            if (cageArray[i].size() == 0) {
                continue;
            }

            // Memanggil method untuk melakukan pengaturan kandang pada class ArrangeCage
            ArrangeCage.arrangement(cageArray[i]);
        }

        // Mencetak informasi jumlah tiap hewan
        System.out.println("\nNUMBER OF ANIMALS:");
        for (int i = 0; i < cageArray.length; i++) {
            System.out.printf("%s:%d\n", availableAnimals[i], cageArray[i].size());
        }

    }

    // Membuat method untuk tahap ketiga, yaitu mengunjungi hewan-hewan
    public static void zooVisit() {
        Scanner input = new Scanner(System.in);
        while (true) {
            System.out.println("Which animal you want to visit?");
            System.out.println("(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: Exit)");
            int in1 = Integer.parseInt(input.nextLine());

            if (in1 == 1) {
                catVisit();

            } else if (in1 == 2) {
                eagleVisit();

            } else if (in1 == 3) {
                hamsterVisit();

            } else if (in1 == 4) {
                parrotVisit();

            } else if (in1 == 5) {
                lionVisit();

            } else if (in1 == 99) {
                break;
            }

            System.out.println("Back to the office!\n");
        }
    }

    // Method untuk mengunjungi kucing, elang, hamster, kakatua dan singa
    // secara berurutan
    public static void catVisit() {
        Scanner input = new Scanner(System.in);
        System.out.print("Mention the name of cat you want to visit: ");
        String catName = input.nextLine();
        // Melakukan pencarian kucing dengan nama sesuai input
        // lalu objek kucing tersebut disimpan dala variabel
        Cage cageFound = findInstance(catCageList, catName);

        // Kondisi jika ada kucing dengan nama sesuai input
        if (cageFound != null) {
            System.out.printf("You are visiting %s (cat) now, ", catName);
            System.out.println("what would you like to do?");
            System.out.println("1: Brush the fur 2: Cuddle");
            int in2 = Integer.parseInt(input.nextLine());

            // Melakukan hal terhadap hewan sesuai input yang dipilih user
            if (in2 == 1) {
                cageFound.getCat().brushTheFur();

            } else if (in2 == 2) {
                cageFound.getCat().cuddle();

            } else {
                System.out.println("You do nothing...");
            }

        // Kondisi jika kucing dengan nama sesuai input tidak ditemukan
        } else {
            System.out.print("There is no cat with that name! ");
        }
    }

    public static void eagleVisit() {
        Scanner input = new Scanner(System.in);
        System.out.print("Mention the name of eagle you want to visit: ");
        String eagleName = input.nextLine();
        Cage cageFound = findInstance(eagleCageList, eagleName);

        if (cageFound != null) {
            System.out.printf("You are visiting %s (eagle) now, ", eagleName);
            System.out.println(" what would you like to do?");
            System.out.println("1: Order to fly");
            int in2 = Integer.parseInt(input.nextLine());

            if (in2 == 1) {
                cageFound.getEagle().fly();

            } else {
                System.out.println("You do nothing...");
            }

        } else {
            System.out.print("There is no eagle with that name! ");
        }
    }

    public static void hamsterVisit() {
        Scanner input = new Scanner(System.in);
        System.out.print("Mention the name of hamster you want to visit: ");
        String hamsterName = input.nextLine();
        Cage cageFound = findInstance(hamsterCageList, hamsterName);

        if (cageFound != null) {
            System.out.printf("You are visiting %s (hamster) now, ", hamsterName);
            System.out.println("what would you like to do?");
            System.out.println("1: See it gnawing 2: Order to run in the hamster wheel");
            int in2 = Integer.parseInt(input.nextLine());
            if (in2 == 1) {
                cageFound.getHamster().gnawing();

            } else if (in2 == 2) {
                cageFound.getHamster().runInTheWheel();

            } else {
                System.out.println("You do nothing...");
            }

        } else {
            System.out.print("There is no hamster with that name! ");
        }
    }

    public static void parrotVisit() {
        Scanner input = new Scanner(System.in);
        System.out.print("Mention the name of parrot you want to visit: ");
        String parrotName = input.nextLine();
        Cage cageFound = findInstance(parrotCageList, parrotName);
        if (cageFound != null) {
            System.out.printf("You are visiting %s (parrot) now, ", parrotName);
            System.out.println("what would you like to do?");
            System.out.println("1: Order to fly 2: Do conversation");
            int in2 = Integer.parseInt(input.nextLine());

            if (in2 == 1) {
                cageFound.getParrot().fly();

            } else if (in2 == 2) {
                System.out.print("You say: ");
                String str = input.nextLine();
                cageFound.getParrot().chat(str);

            } else {
                cageFound.getParrot().chat("");
            }

        } else {
            System.out.print("There is no parrot with that name! ");
        }
    }

    public static void lionVisit() {
        Scanner input = new Scanner(System.in);
        System.out.print("Mention the name of lion you want to visit: ");
        String lionName = input.nextLine();
        Cage cageFound = findInstance(lionCageList, lionName);
        if (cageFound != null) {
            System.out.printf("You are visiting %s (lion) now, ", lionName);
            System.out.println("what would you like to do?");
            System.out.println("1: See it hunting 2: Brush the mane 3: Disturb it");
            int in2 = Integer.parseInt(input.nextLine());

            if (in2 == 1) {
                cageFound.getLion().hunting();

            } else if (in2 == 2) {
                cageFound.getLion().brushTheMane();

            } else if (in2 == 3) {
                cageFound.getLion().disturbed();

            } else {
                System.out.println("You do nothing...");
            }

        } else {
            System.out.print("There is no lion with that name! ");
        }
    }

    // Membuat method untuk melakukan pengecekan suatu objek dengan input berupa String
    private static Cage findInstance(ArrayList<Cage> cages, String str) {
        for (Cage cage : cages) {
            if (cage.getCat() != null) {
                String catName = cage.getCat().getName();
                if (catName.equals(str)) {
                    return cage;
                }

            } else if (cage.getEagle() != null) {
                String eagleName = cage.getEagle().getName();
                if (eagleName.equals(str)) {
                    return cage;
                }

            } else if (cage.getHamster() != null) {
                String hamsterName = cage.getHamster().getName();
                if (hamsterName.equals(str)) {
                    return cage;
                }

            } else if (cage.getParrot() != null) {
                String parrotName = cage.getParrot().getName();
                if (parrotName.equals(str)) {
                    return cage;
                }

            } else if (cage.getLion() != null) {
                String lionName = cage.getLion().getName();
                if (lionName.equals(str)) {
                    return cage;
                }
            }
        }
        return null;
    }

}

/**
* Class Hamster untuk membuat objek hamster.
* Merupakan subclass dari class Animal
*/

package animal;

public class Hamster extends Animal {
    // Membuat variabel yang menyimpan tipe kandang
    private String cageClass;

    public Hamster(String name, int length) {
        // Menggunakan constructor yang ada pada superclass
        super(name, length);

        // Menentukan tipe kandang berdasarkan kategori pada lokasi indoor
        // karena merupakan hewan jinak
        if (length < 45) {
            this.cageClass = "A";
        } else if (length >= 45 && length <= 60) {
            this.cageClass = "B";
        } else {
            this.cageClass = "C";
        }
    }

    // Membuat method untuk melihat hamster yang sedang makan sesuai format
    public void gnawing() {
        System.out.printf("%s makes a voice: ngkkrit.. ngkkrrriiit\n", this.name);
    }

    // Membuat method untuk memerintahkan hamster berlari di roda sesuai format
    public void runInTheWheel() {
        System.out.printf("%s makes a voice: trrr.. trrr...\n", this.name);
    }

    public String getCageClass() {
        return this.cageClass;
    }
}

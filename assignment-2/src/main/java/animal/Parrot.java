/**
* Class Parrot untuk membuat objek burung kakatua.
* Merupakan subclass dari class Animal
*/

package animal;

public class Parrot extends Animal {
    // Membuat variabel yang menyimpan tipe kandang
    private String cageClass;

    public Parrot(String name, int length) {
        // Menggunakan constructor yang ada pada superclass
        super(name, length);

        // Menentukan tipe kandang berdasarkan kategori pada lokasi indoor
        // karena merupakan hewan jinak
        if (length < 45) {
            this.cageClass = "A";
        } else if (length >= 45 && length <= 60) {
            this.cageClass = "B";
        } else {
            this.cageClass = "C";
        }
    }

    // Membuat method untuk burung kakatua terbang sesuai format
    public void fly() {
        System.out.printf("Parrot %s flies!\n", this.name);
        System.out.printf("%s makes a voice: FLYYYY...\n", this.name);
    }

    // Membuat method untuk mengajak burung kakatua mengobrol
    public void chat(String str) {
        if (str.length() > 0) {
            System.out.printf("%s says: %s\n", this.name, str.toUpperCase());
        } else {
            System.out.printf("%s says: HM?\n", this.name);
        }
    }

    public String getCageClass() {
        return this.cageClass;
    }

}

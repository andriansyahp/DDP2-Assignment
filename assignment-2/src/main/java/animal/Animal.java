/**
* Class yang berfungsi sebagai superclass untuk class-class hewan.
*/

package animal;

public class Animal {
    // Membuat variabel-variabel yang menyimpan nama serta panjang tubuh hewan
    // Menggunakan access modifier protected agar dapat diakses oleh subclass
    protected String name;
    protected int length;

    // Membuat constructor
    public Animal(String name, int length) {
        this.name = name;
        this.length = length;
    }

    public String getName() {
        return this.name;
    }

    public int getLength() {
        return this.length;
    }
}

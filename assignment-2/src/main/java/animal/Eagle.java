/**
* Class Eagle untuk membuat objek elang.
* Merupakan subclass dari class Animal
*/

package animal;

public class Eagle extends Animal {
    // Membuat variabel yang menyimpan tipe kandang
    private String cageClass;

    public Eagle(String name, int length) {
        // Menggunakan constructor yang ada pada superclass
        super(name, length);

        // Menentukan tipe kandang berdasarkan kategori pada lokasi outdoor
        // karena merupakan hewan liar
        if (length < 75) {
            this.cageClass = "A";
        } else if (length >= 75 && length <= 90) {
            this.cageClass = "B";
        } else {
            this.cageClass = "C";
        }
    }

    // Membuat fungsi untuk terbang sesuai format
    public void fly() {
        System.out.printf("%s makes a voice: kwaakk...\n", this.name);
        System.out.println("You hurt!");
    }

    public String getCageClass() {
        return this.cageClass;
    }
}

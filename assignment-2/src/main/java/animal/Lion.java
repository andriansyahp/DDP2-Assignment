/**
* Class Lion untuk membuat objek singa.
* Merupakan subclass dari class Animal
*/

package animal;

public class Lion extends Animal {
    // Membuat variabel yang menyimpan tipe kandang
    private String cageClass;

    public Lion(String name, int length) {
        // Menggunakan constructor yang ada pada superclass
        super(name, length);

        // Menentukan tipe kandang berdasarkan kategori pada lokasi outdoor
        // karena merupakan hewan liar
        if (length < 75) {
            this.cageClass = "A";
        } else if (length >= 75 && length <= 90) {
            this.cageClass = "B";
        } else {
            this.cageClass = "C";
        }
    }

    // Membuat method untuk singa berburu sesuai format
    public void hunting() {
        System.out.println("Lion is hunting..");
        System.out.printf("%s makes a voice: err...!\n", this.name);
    }

    // Membuat method untuk membersihkan bulu singa sesuai format
    public void brushTheMane() {
        System.out.println("Clean the lion’s mane..");
        System.out.printf("%s makes a voice: Hauhhmm!\n", this.name);
    }

    // Membuat method bagi singa jika diganggu sesuai format
    public void disturbed() {
        System.out.printf("%s makes a voice: HAUHHMM!\n", this.name);
    }

    public String getCageClass() {
        return this.cageClass;
    }

}

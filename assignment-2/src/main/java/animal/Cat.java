/**
* Class Cat untuk membuat objek kucing.
* Merupakan subclass dari class Animal
*/

package animal;

import java.util.Random;

public class Cat extends Animal {
    // Membuat variabel yang menyimpan tipe kandang
    private String cageClass;
    // Mmebuat array yang menampung kemungkinan respon kucing saat dipeluk
    private String[] cuddleResponse = {"Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!"};

    public Cat(String name, int length) {
        // Menggunakan constructor yang ada pada superclass
        super(name, length);

        // Menentukan tipe kandang berdasarkan kategori pada lokasi indoor
        // karena merupakan hewan jinak
        if (length < 45) {
            this.cageClass = "A";
        } else if (length >= 45 && length <= 60) {
            this.cageClass = "B";
        } else {
            this.cageClass = "C";
        }
    }

    // Membuat method untuk membersihkan bulu kucing sesuai format
    public void brushTheFur() {
        System.out.printf("Time to clean %s's fur\n", this.name);
        System.out.printf("%s makes a voice: Nyaaan...\n", this.name);
    }

    // Membuat method untuk memeluk kucing yang menghasilkan output secara random
    public void cuddle() {
        Random random = new Random();
        int randIndex = random.nextInt(4);
        System.out.printf("%s makes a voice: %s\n", this.name, this.cuddleResponse[randIndex]);
    }

    public String getCageClass() {
        return this.cageClass;
    }

}

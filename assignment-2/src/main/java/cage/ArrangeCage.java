/**
* Class untuk melakukan pengaturan pada tiap cage yang diberikan.
*/

package cage;

import cage.Cage;
import java.util.ArrayList;
import java.util.Arrays;

public class ArrangeCage {
    // Membuat method untuk mengatur kandang
    public static void arrangement(ArrayList<Cage> listOfCages) {
        // Melakukan pengaturan kandang yang pertama, yaitu pengaturan awal
        // Kemudian hasil pengaturannya disimpan dalam array
        Cage[][] firstArranged = levelArrangement(listOfCages);

        // Melakukan pencetakan sesuai format
        System.out.printf("\nlocation: %s\n", listOfCages.get(0).getLocation());
        for (int i = 3; i > 0; i--) {
            // Memanggil method untuk melakukan pencetakan
            printCages(firstArranged[i - 1], i);
        }
        System.out.println("\nAfter rearrangement...");
        // Melakukan pengaturan kandang kedua sesuai pola pada soal
        // Kemudian disimpan hasilnya dalam array, lalu dicetak sesuai format
        Cage[][] secondArranged = levelRearrangement(firstArranged);
        for (int i = 3; i > 0; i--) {
            printCages(secondArranged[i - 1], i);
        }
    }

    // Membuat method untuk pengaturan awal kandang
    private static Cage[][] levelArrangement(ArrayList<Cage> animalCages) {
        // Menentukan terlebih dahulu jumlah anggota dari tiap level
        // melalui method levelSize()
        int a = levelSize(animalCages.size(), 1);
        int b = levelSize(animalCages.size(), 2);
        int c = levelSize(animalCages.size(), 3);

        // Menginisiasi level berupa array yang akan diisi oleh kandang-kandang hewan
        Cage[] level1 = new Cage[a];
        Cage[] level2 = new Cage[b];
        Cage[] level3 = new Cage[c];

        // Melakukan for-loop untuk mengisi array tiap level
        for (int i = 0, j = 0, k = 0; i < animalCages.size(); i++) {
            // Membuat kondisi ketika level 1 belum terisi penuh
            if (i < level1.length) {
                level1[i] = animalCages.get(i);

            // Membuat kondisi ketika level 1 sudah terisi penuh
            // dan masuk ke level 2
            } else if (i < level2.length + level1.length) {
                level2[j] = animalCages.get(i);
                j += 1;

            // Membuat kondisi ketika level 2 sudah terisi penuh
            // kemudian masuk ke level 3
            } else {
                level3[k] = animalCages.get(i);
                k += 1;
            }
        }

        // Membuat array yang menampung hasil pengaturan kandang
        // untuk selanjutnya dikembalikan
        Cage[][] arranged = new Cage[3][];
        arranged[0] = new Cage[level1.length];
        arranged[0] = level1;
        arranged[1] = new Cage[level2.length];
        arranged[1] = level2;
        arranged[2] = new Cage[level3.length];
        arranged[2] = level3;

        return arranged;
    }

    // Membuat method untuk menentukan jumlah anggota tiap level
    private static int levelSize(int index, int level) {
        // Membuat kondisi jika index sekarang merupakan kelipatan 3
        if (index % 3 == 0) {
            // Mengembalikan hasil pembagian index dengan 3 (membagi rata untuk tiap level)
            return index / 3;

        // Membuat kondisi jika index sekarang dibagi 3 akan menghasilkan sisa 1
        } else if (index % 3 == 1) {
            // Kondisi normal ketika level 1 dan 2
            if (level <= 2) {
                // Mengembalikan hasil bagi index dengan 3
                return index / 3;
            // Kondisi ketika level 3
            } else {
                // Mengembalikan hasil ceiling division dari index dengan 3
                return index / 3 + 1;
            }

        // Membuat kondisi jika index sekarang dibagi 3 akan menghasilkan sisa 2
        } else if (index % 3 == 2) {
            if (level <= 2) {
                // Mengembalikan hasil ceiling division oleh index dengan 3
                return index / 3 + 1;
            } else {
                // Mengembalikan sisa pembagian index dengan 3, yaitu 2
                return 2;
            }
        }

        // Mengembalikan 0 sebagai default return
        return 0;
    }

    // Membuat method untuk mencetak hasil pengaturan kandang sesuai format
    private static void printCages(Cage[] arr, int i) {
        System.out.printf("level %d: ", i);
        for (Cage cage : arr) {
            if (cage == null) {
                continue;
            }
            System.out.print(cage + ", ");
        }
        System.out.println();
    }

    // Membuat method pengaturan akhir kandang
    private static Cage[][] levelRearrangement(Cage[][] arr) {
        // Menentukan jumlah anggota tiap level seperti pengaturan awal
        // kemudian menginisiasi array untuk menampung kandang tiap level
        int level1 = arr[0].length;
        int level2 = arr[1].length;
        int level3 = arr[2].length;
        Cage[] level1Rearranged = new Cage[level3];
        Cage[] level2Rearranged = new Cage[level1];
        Cage[] level3Rearranged = new Cage[level2];

        // Melakukan reverse pada kandang di tiap level,
        // sekaligus menaikkan 1 level pada tiap level sesuai pola pada soal
        for (int i = 0, j = level3 - 1; i < level3; i++, j--) {
            level1Rearranged[i] = arr[2][j];
        }
        for (int i = 0, j = level1 - 1; i < level1; i++, j--) {
            level2Rearranged[i] = arr[0][j];
        }
        for (int i = 0, j = level2 - 1; i < level2; i++, j--) {
            level3Rearranged[i] = arr[1][j];
        }

        // Membuat array untuk menampung hasil pengaturan akhir kandang
        // yang kemudian selanjutnya akan dikembalikan
        Cage[][] arrayRearranged = new Cage[3][];
        arrayRearranged[0] = new Cage[level1Rearranged.length];
        arrayRearranged[0] = level1Rearranged;
        arrayRearranged[1] = new Cage[level2Rearranged.length];
        arrayRearranged[1] = level2Rearranged;
        arrayRearranged[2] = new Cage[level3Rearranged.length];
        arrayRearranged[2] = level3Rearranged;

        return arrayRearranged;
    }
}

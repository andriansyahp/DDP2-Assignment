/**
* Class Cage untuk membuat kandang bagi setiap hewan.
*/

package cage;

import animal.Cat;
import animal.Eagle;
import animal.Hamster;
import animal.Lion;
import animal.Parrot;
import java.util.ArrayList;

public class Cage {
    private Cat cat;
    private Eagle eagle;
    private Hamster hamster;
    private Parrot parrot;
    private Lion lion;
    private String location;

    // Membuat constructors yang overloaded, bergantung pada tipe parameter yang diberikan
    public Cage(Cat cat) {
        this.cat = cat;
        this.location = "indoor";
    }

    public Cage(Eagle eagle) {
        this.eagle = eagle;
        this.location = "outdoor";
    }

    public Cage(Hamster hamster) {
        this.hamster = hamster;
        this.location = "indoor";
    }

    public Cage(Parrot parrot) {
        this.parrot = parrot;
        this.location = "indoor";
    }

    public Cage(Lion lion) {
        this.lion = lion;
        this.location = "outdoor";
    }

    public Cat getCat() {
        return this.cat;
    }

    public Eagle getEagle() {
        return this.eagle;
    }

    public Hamster getHamster() {
        return this.hamster;
    }

    public Parrot getParrot() {
        return this.parrot;
    }

    public Lion getLion() {
        return this.lion;
    }

    public String getLocation() {
        return this.location;
    }

    // Meng-override method untuk mencetak objek kandang
    // untuk mempermudah saat diolah di class ArrangeCage
    public String toString() {
        // Melakukan seleksi jenis hewan
        if (this.cat != null) {
            String name = cat.getName();
            int length = cat.getLength();
            String cageClass = cat.getCageClass();
            return String.format("%s (%d - %s)", name, length, cageClass);
        } else if (this.eagle != null) {
            String name = eagle.getName();
            int length = eagle.getLength();
            String cageClass = eagle.getCageClass();
            return String.format("%s (%d - %s)", name, length, cageClass);
        } else if (this.hamster != null) {
            String name = hamster.getName();
            int length = hamster.getLength();
            String cageClass = hamster.getCageClass();
            return String.format("%s (%d - %s)", name, length, cageClass);
        } else if (this.parrot != null) {
            String name = parrot.getName();
            int length = parrot.getLength();
            String cageClass = parrot.getCageClass();
            return String.format("%s (%d - %s)", name, length, cageClass);
        } else {
            String name = lion.getName();
            int length = lion.getLength();
            String cageClass = lion.getCageClass();
            return String.format("%s (%d - %s)", name, length, cageClass);
        }
    }

}

package javarimatch;

import javax.swing.*;


/**
 * Javari Animal Match - Mythical Edition!
 * A memory match-pair game starring mythological animals
 * from the Greeks, Norse and all over the world.
 */
public class MainGame{
    private static void initGame() {
        GameFrame mainGame = new GameFrame();
        // Make the frame to be full-screen from the first time the program run.
        mainGame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        mainGame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        //Display the window.
        mainGame.pack();
        mainGame.setVisible(true);
    }

    public static void main(String[] args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        SwingUtilities.invokeLater(MainGame::initGame);
    }
}
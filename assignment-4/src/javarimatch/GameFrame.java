package javarimatch;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;

/**
 * A class made to manage the memory game.
 * Taking control over the rules of the game,
 * as well as being the container for the GUI parts.
 */
class GameFrame extends JFrame{
    /**
     * ImageIcon instance variables,
     * used for the icon of the buttons.
     */
    private ImageIcon centaur;
    private ImageIcon cerberus;
    private ImageIcon chimera;
    private ImageIcon cthulhu;
    private ImageIcon fenrir;
    private ImageIcon gryffin;
    private ImageIcon hydra;
    private ImageIcon kappa;
    private ImageIcon kirin;
    private ImageIcon kraken;
    private ImageIcon kyuubi;
    private ImageIcon leviathan;
    private ImageIcon medusa;
    private ImageIcon minotaur;
    private ImageIcon pegasus;
    private ImageIcon phoenix;
    private ImageIcon unicorn;
    private ImageIcon yeti;
    private ImageIcon logo = fitTheImage(new ImageIcon(getClass().getResource("/animals/logo.png")));

    /**
     * Variable for keeping up with the amount of steps taken by
     * the player in order to complete the game.
     */
    private int totalSteps = 0;

    /**
     * ArrayList containing cards and the generated IDs,
     * used for matching of the cards later on.
     */
    private ArrayList<Card> cards = new ArrayList<>();
    private ArrayList<Integer> cardIdList = new ArrayList<>();

    /**
     * Timer for keeping up with the time in the transition of
     * revealing cards by the player.
     */
    private Timer timer;

    /**
     * Variable to know which card(s) is/are selected
     */
    private Card currentCard;
    private Card cardA;
    private Card cardB;

    /**
     * Variable to store the arguments for
     * removing borders in JButton.
     */
    private Border emptyBorder = BorderFactory.createEmptyBorder();

    /**
     * Variable to show the label
     * containing the amounts of steps taken by the
     * player while playing the game.
     */
    private JLabel attemptsLabel;


    /**
     * Constructor. Initializing the creation of the cards and the icons,
     * as well as the timer and also the the initiation of the
     * user interface to be implemented.
     */
    GameFrame(){
        initCard();

        initIcon();

        timer = new Timer(750, ae -> checkMatch());
        timer.setRepeats(false);

        initUI();
    }

    /**
     * Initiation of the icons, making use of the ImageIcon
     * but first, it has to be resized in order to make it
     * look more compact and solid in the button.
     */
    private void initIcon() {
        centaur   = fitTheImage(new ImageIcon(getClass().getResource("/animals/centaur.png")));
        cerberus  = fitTheImage(new ImageIcon(getClass().getResource("/animals/cerberus.png")));
        chimera   = fitTheImage(new ImageIcon(getClass().getResource("/animals/chimera.png")));
        cthulhu   = fitTheImage(new ImageIcon(getClass().getResource("/animals/cthulhu.png")));
        fenrir    = fitTheImage(new ImageIcon(getClass().getResource("/animals/fenrir.png")));
        gryffin   = fitTheImage(new ImageIcon(getClass().getResource("/animals/gryffin.png")));
        hydra     = fitTheImage(new ImageIcon(getClass().getResource("/animals/hydra.png")));
        kappa     = fitTheImage(new ImageIcon(getClass().getResource("/animals/kappa.png")));
        kirin     = fitTheImage(new ImageIcon(getClass().getResource("/animals/kirin.png")));
        kraken    = fitTheImage(new ImageIcon(getClass().getResource("/animals/kraken.png")));
        kyuubi    = fitTheImage(new ImageIcon(getClass().getResource("/animals/kyuubi.png")));
        leviathan = fitTheImage(new ImageIcon(getClass().getResource("/animals/leviathan.png")));
        medusa    = fitTheImage(new ImageIcon(getClass().getResource("/animals/medusa.png")));
        minotaur  = fitTheImage(new ImageIcon(getClass().getResource("/animals/minotaur.png")));
        pegasus   = fitTheImage(new ImageIcon(getClass().getResource("/animals/pegasus.png")));
        phoenix   = fitTheImage(new ImageIcon(getClass().getResource("/animals/phoenix.png")));
        unicorn   = fitTheImage(new ImageIcon(getClass().getResource("/animals/unicorn.png")));
        yeti      = fitTheImage(new ImageIcon(getClass().getResource("/animals/yeti.png")));
    }

    /**
     * Initiation of the creation of cards.
     */
    private void initCard() {
        // Store integers in range of 0-17 to make it as the identifier of the cards
        // Added twice in the cardIdList because it will be in form of pairs (18 x 2 = 6 x 6 grid)
        for (int index = 0; index < 18; index++){
            cardIdList.add(index);
            cardIdList.add(index);
        }
        // Make it shuffled so the order is randomized
        Collections.shuffle(cardIdList);

        // Initiating the buttons, also configuring its properties
        for (int val : cardIdList){
            Card card = new Card(val);
            card.setIcon(logo);
            card.setBackground(Color.GRAY);
            card.setBorder(emptyBorder);

            // Adding ActionListener utilizing lambda expression
            card.addActionListener(actionEvent -> {
                currentCard = card;
                cardButtonPressed();
            });
            cards.add(card);
        }
    }

    /**
     * Parts of the program that is in charge for
     * the User Interface of the program.
     */
    private void initUI() {
        setTitle("Javari Animal Match - Mythical Edition!");

        // Creating first panel to hold the cards as a grid ordered buttons
        JPanel cardPanel = new JPanel();
        cardPanel.setLayout(new GridLayout(6, 6));
        cardPanel.setBackground(Color.BLACK);
        cardPanel.setPreferredSize(new Dimension(680, 680));
        for (Card card : cards){
            cardPanel.add(card);
        }

        // Creating second panel to hold the "Quit" button,
        // "Play Again" button and the "Steps Taken" label.
        JPanel miscellanousPanel = new JPanel();
        miscellanousPanel.setLayout(new BorderLayout());
        miscellanousPanel.setBackground(Color.BLACK);
        miscellanousPanel.setPreferredSize(new Dimension(200, 50));

        JButton quitButton = new JButton("QUIT");
        quitButton.setPreferredSize(new Dimension(80, 50));
        quitButton.setFont(new Font("Serif", Font.BOLD, 22));
        quitButton.setBackground(Color.RED);
        quitButton.setForeground(Color.WHITE);
        quitButton.setBorder(emptyBorder);
        quitButton.addActionListener(actionEvent -> quitButtonPressed());

        JButton resetButton = new JButton("PLAY AGAIN");
        resetButton.setPreferredSize(new Dimension(150, 50));
        resetButton.setFont(new Font("Serif", Font.BOLD, 22));
        resetButton.setBackground(Color.GREEN);
        resetButton.setForeground(Color.WHITE);
        resetButton.setBorder(emptyBorder);
        resetButton.addActionListener(actionEvent -> resetButtonPressed());

        String strToShow = String.format("Steps taken: %d", totalSteps);
        attemptsLabel = new JLabel(strToShow, JLabel.CENTER);
        attemptsLabel.setFont(new Font("Serif", Font.BOLD, 22));
        attemptsLabel.setForeground(Color.WHITE);

        miscellanousPanel.add(resetButton, BorderLayout.LINE_START);
        miscellanousPanel.add(quitButton, BorderLayout.LINE_END);
        miscellanousPanel.add(attemptsLabel, BorderLayout.CENTER);

        Container pane = getContentPane();
        pane.setLayout(new BorderLayout());
        pane.add(cardPanel, BorderLayout.PAGE_START);
        pane.add(miscellanousPanel, BorderLayout.CENTER);
    }

    /**
     * Method for the "Quit" button ActionListener event.
     * Close the program if the button pressed.
     */
    private void quitButtonPressed() {
        this.dispose();
    }

    /**
     * Method for the "Play Again" button ActionListener event.
     * Reset the condition of the game to the initial one.
     */
    private void resetButtonPressed() {
        Collections.shuffle(cardIdList);
        for (int i = 0; i < cards.size(); i++) {
            cards.get(i).setCardId(cardIdList.get(i));
            cards.get(i).setIcon(logo);
            cards.get(i).setChecked(false);
            cards.get(i).setEnabled(true);
        }
        totalSteps = 0;
        updateAttemptsLabel();
    }

    /**
     * Method for the "Card" buttons ActionListener event.
     */
    private void cardButtonPressed() {
        // Check if this is the first card (to be matched) that is being pressed
        if (cardA == null && cardB == null) {
            cardA = currentCard;
            ImageIcon icon = findIcon(cardA.getCardId());
            cardA.setIcon(icon);
        }
        // Only accepts condition if it is the second card (to be matched) that is being pressed,
        // Also taking notes that the same card cannot be pressed twice (to match with the card itself).
        if (cardA != null && cardA != currentCard && cardB == null) {
            cardB = currentCard;
            ImageIcon icon = findIcon(cardB.getCardId());
            cardB.setIcon(icon);
            // Starts the countdown to close the cards again.
            timer.start();
        }
    }

    /**
     * Method to check whether the two cards chosen is matched or not.
     */
    private void checkMatch(){
        // Update the amount of steps taken by the player
        // that is being stored in the label when it is the time
        // for the matching (2 cards chosen is counted as one move).
        totalSteps++;
        updateAttemptsLabel();

        // Condition if the ID of the 2 cards matched.
        if (cardA.getCardId() == cardB.getCardId()) {
            cardA.setEnabled(false);
            cardA.setChecked(true);

            cardB.setEnabled(false);
            cardB.setChecked(true);

            // Check if the game is already finished.
            if (cardsCleared()) {
                JOptionPane.showMessageDialog(this, "You completed the game with " + totalSteps + " steps!");
            }

        } else {
            // Re-close the cards if the ID of the 2 cards doesn't match.
            cardA.setIcon(logo);
            cardB.setIcon(logo);
        }

        // Reset the cards stored, used for the next matching.
        cardA = null;
        cardB = null;
    }

    /**
     * Method to check if the game is already finished.
     * @return boolean condition of each of every cards matched condition.
     */
    private boolean cardsCleared() {
        for (Card card: this.cards) {
            if (!card.getChecked()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Method to resize the image.
     * @param icon Image that is wished to be resized.
     * @return the result of the resized image.
     */
    private ImageIcon fitTheImage(ImageIcon icon) {
        Image img = icon.getImage() ;
        Image newImg = img.getScaledInstance(250, 120, Image.SCALE_SMOOTH) ;
        icon = new ImageIcon(newImg);
        return icon;
    }

    /**
     * Method to depicts an "Animal" in form of a cards,
     * corresponded to the ID listed below.
     * @param id the ID of the "Animal" (card).
     * @return the ImageIcon corresponds to the ID.
     */
    private ImageIcon findIcon(int id) {
        ImageIcon retIcon = null;
        switch(id) {
            case 0:
                retIcon = centaur;
                break;
            case 1:
                retIcon = cerberus;
                break;
            case 2:
                retIcon = chimera;
                break;
            case 3:
                retIcon = cthulhu;
                break;
            case 4:
                retIcon = fenrir;
                break;
            case 5:
                retIcon = gryffin;
                break;
            case 6:
                retIcon = hydra;
                break;
            case 7:
                retIcon = kappa;
                break;
            case 8:
                retIcon = kirin;
                break;
            case 9:
                retIcon = kraken;
                break;
            case 10:
                retIcon = kyuubi;
                break;
            case 11:
                retIcon = leviathan;
                break;
            case 12:
                retIcon = medusa;
                break;
            case 13:
                retIcon = minotaur;
                break;
            case 14:
                retIcon = pegasus;
                break;
            case 15:
                retIcon = phoenix;
                break;
            case 16:
                retIcon = unicorn;
                break;
            case 17:
                retIcon = yeti;
                break;
        }
        return retIcon;
    }

    /**
     * Method to update the Label of "Steps Taken",
     * each time in every match-up taking place.
     */
    private void updateAttemptsLabel() {
        String strShow = String.format("Steps taken: %d", totalSteps);
        attemptsLabel.setText(strShow);
    }
}


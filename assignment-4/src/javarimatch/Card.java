package javarimatch;

import javax.swing.JButton;

/**
 * Class to store unique ID of the cards,
 * as a way to differ one card from the other.
 */
public class Card extends JButton {
    /**
     * The unique ID to identify the card.
     */
    private int id;

    /**
     * The boolean condition whether the card already found its match or not.
     */
    private boolean isChecked;

    /**
     * Constructor.
     * @param id The unique ID
     */
    Card(int id){
        this.id = id;
    }

    /**
     * Method to get the ID of a card.
     * @return the ID of the card.
     */
    public int getCardId() {
        return this.id;
    }

    /**
     * Method to change the ID of the card,
     * specifically used when resetting the game
     * (when the ID of the cards is being reshuffled).
     * @param newId the new ID for the card.
     */
    public void setCardId(int newId) {
        this.id = newId;
    }

    /**
     * Method to access the matched state of the card.
     * @return the boolean state of the card matching.
     */
    public boolean getChecked() {
        return this.isChecked;
    }

    /**
     * Method to change the matched state of the card.
     * @param check the boolean state wished to be assigned to the card.
     */
    public void setChecked(boolean check) {
        this.isChecked = check;
    }

}

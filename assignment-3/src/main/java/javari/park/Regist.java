package javari.park;

import java.util.ArrayList;

public class Regist{
	private String name;
	private int id;
	private static int staticID = 0;
	private ArrayList<SelectedAttraction> attraction = new ArrayList<SelectedAttraction>();

	public Regist(String name) {
		this.name = name;
		this.id = staticID++;
	}

	public int getRegistrationId() {
		return this.id;
	}

	public String getVisitorName() {
		return this.name;
	}

	public void setVisitorName(String name) {
		this.name = name;
	}

	public ArrayList<SelectedAttraction> getSelectedAttractions() {
		return this.attraction;
	}

}

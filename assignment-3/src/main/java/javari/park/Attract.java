package javari.park;

import javari.animal.*;
import java.util.ArrayList;

public class Attract implements SelectedAttraction{
	private String name;
	private String type;
	private ArrayList<Animal> performers = new ArrayList<Animal>();

	public Attract(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public String getType() {
		return this.type;
	}

	public ArrayList<Animal> getPerformers(){
		return this.performers;
	}

}

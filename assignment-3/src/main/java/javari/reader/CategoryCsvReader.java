package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;

import javari.animal.*;



public class CategoryCsvReader extends CsvReader{
    static final String[] AVAILABLE_CATEGORY = {"Mammals", "Aves", "Reptiles"};
    private ArrayList<String> availableCategories = new ArrayList<String>();

	public CategoryCsvReader(Path file) throws IOException {
		super(file);
	}

	@Override
	public long countValidRecords() {
		long countValidLines = 0;

		for (String line : super.lines) {
			String[] lineSplit = line.split(COMMA);
			if (lineSplit[2].equalsIgnoreCase("explore the mammals")) {
				if (lineSplit[1].equalsIgnoreCase("mammals") && findCategory(lineSplit[1]) == false) {
					countValidLines += 1;
                    this.availableCategories.add("mammals");
				}
			} else if (lineSplit[2].equalsIgnoreCase("world of aves")) {
				if (lineSplit[1].equalsIgnoreCase("aves") && findCategory(lineSplit[1]) == false) {
					countValidLines += 1;
                    this.availableCategories.add("aves");
				}
			} else if (lineSplit[2].equalsIgnoreCase("reptillian kingdom")) {
				if (lineSplit[1].equalsIgnoreCase("reptiles") && findCategory(lineSplit[1]) == false) {
					countValidLines += 1;
                    this.availableCategories.add("reptiles");
				}
			}
		}

        this.resetAvailableCategories();
		return countValidLines;
	}

	@Override
	public long countInvalidRecords() {
        long validLines = countValidRecords();
        long invalidLines = AVAILABLE_CATEGORY.length - validLines;
        return invalidLines;
	}

    private boolean findCategory(String category) {
        for (String ctgry : this.availableCategories) {
            if (category.equalsIgnoreCase(ctgry)) {
                return true;
            }
        }
        return false;
    }

    private void resetAvailableCategories() {
        for (int i = 0; i < this.availableCategories.size() + 2; i++) {
            this.availableCategories.remove(0);
        }
    }
}

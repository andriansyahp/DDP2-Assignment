package javari.reader;

import java.io.IOException;
import java.nio.file.Path;

import java.util.ArrayList;
import javari.animal.*;


public class RecordCsvReader extends CsvReader {
	private static ArrayList<Animal> animals = new ArrayList<Animal>();

	public RecordCsvReader(Path file) throws IOException {
		super(file);
	}

	public ArrayList<Animal> getAnimals() {
		return this.animals;
	}

	@Override
	public long countValidRecords() {
		long countValidLines = 0;

		for (String line : super.lines) {
			String[] lineSplit = line.split(COMMA);
			if (Mammal.MAMMALS_TYPE.contains(lineSplit[1].toLowerCase())) {
			    boolean specialCondition = lineSplit[6].equalsIgnoreCase("pregnant");
			    Animal animal = new Mammal(Integer.parseInt(lineSplit[0]), lineSplit[1],
                        lineSplit[2], Gender.parseGender(lineSplit[3]),
                        Double.parseDouble(lineSplit[4]), Double.parseDouble(lineSplit[5]),
                        specialCondition, Condition.parseCondition(lineSplit[7]));
			    if (animal.isShowable()) {
                    animals.add(animal);
                }
			    countValidLines += 1;
			} else if (Reptile.REPTILIAN_TYPE.contains(lineSplit[1].toLowerCase())) {
			    boolean specialCondition = lineSplit[6].equalsIgnoreCase("tame");
			    Animal animal = new Reptile(Integer.parseInt(lineSplit[0]), lineSplit[1],
                        lineSplit[2], Gender.parseGender(lineSplit[3]),
                        Double.parseDouble(lineSplit[4]), Double.parseDouble(lineSplit[5]),
                        specialCondition, Condition.parseCondition(lineSplit[7]));
                if (animal.isShowable()) {
                    animals.add(animal);
                }
                countValidLines += 1;
			} else if (Aves.AVES_TYPE.contains(lineSplit[1].toLowerCase())) {
			    boolean specialCondition = lineSplit[6].equalsIgnoreCase("laying eggs");
			    Animal animal = new Aves(Integer.parseInt(lineSplit[0]), lineSplit[1],
                        lineSplit[2], Gender.parseGender(lineSplit[3]),
                        Double.parseDouble(lineSplit[4]), Double.parseDouble(lineSplit[5]),
                        specialCondition, Condition.parseCondition(lineSplit[7]));
                if (animal.isShowable()) {
                    animals.add(animal);
                }
                countValidLines += 1;
			}
		}
		return countValidLines;
	}

	@Override
	public long countInvalidRecords() {
        long validLines = countValidRecords();
        long invalidLines = super.lines.size() - validLines;
        return invalidLines;
	}

}

package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;

public class SectionCsvReader extends CsvReader{
    static final String[] AVAILABLE_SECTIONS = {"Explore the Mammals", "World of Aves", "Reptillian Kingdom"};
    private ArrayList<String> availableSections = new ArrayList<String>();

	public SectionCsvReader(Path file) throws IOException {
		super(file);
	}

	@Override
	public long countValidRecords() {
		long countValidLines = 0;

		for (String line : super.lines) {
			String[] lineSplit = line.split(COMMA);
			if (lineSplit[2].equalsIgnoreCase("explore the mammals")) {
				if (lineSplit[1].equalsIgnoreCase("mammals") && findSection(lineSplit[2]) == false) {
					countValidLines += 1;
					this.availableSections.add("explore the mammals");
				}
			} else if (lineSplit[2].equalsIgnoreCase("world of aves")) {
                if (lineSplit[1].equalsIgnoreCase("aves") && findSection(lineSplit[2]) == false) {
                    countValidLines += 1;
                    this.availableSections.add("world of aves");
                }
			} else if (lineSplit[2].equalsIgnoreCase("reptillian kingdom")) {
                if (lineSplit[1].equalsIgnoreCase("reptiles") && findSection(lineSplit[2]) == false) {
                    countValidLines += 1;
                    this.availableSections.add("reptillian kingdom");
                }
			}
		}

		this.resetAvailableSections();
		return countValidLines;
	}

	@Override
	public long countInvalidRecords() {
	    long validLines = countValidRecords();
	    long invalidLines = AVAILABLE_SECTIONS.length - validLines;
		return invalidLines;
	}

	private boolean findSection(String section) {
	    for (String sect : availableSections) {
            if (section.equalsIgnoreCase(sect)) {
                return true;
            }
        }
        return false;
    }

    private void resetAvailableSections() {
	    for (int i = 0; i < this.availableSections.size() + 2; i++) {
	        this.availableSections.remove(0);
        }
    }

}

package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;

public class AttractionCsvReader extends CsvReader implements AttractionsType{
    private ArrayList<String> availableAttractions = new ArrayList<String>();

	public AttractionCsvReader(Path file) throws IOException {
		super(file);
	}

	@Override
	public long countValidRecords() {
		long countValidLines = 0;

		for (String line : super.lines) {
			String[] lineSplit = line.split(COMMA);
			if (lineSplit[1].equalsIgnoreCase("circles of fires")) {
				if (AttractionsType.CIRCLES_OF_FIRE.contains(lineSplit[0].toLowerCase()) && findAttraction(lineSplit[1]) == false) {
					countValidLines += 1;
                    this.availableAttractions.add("circles of fires");
				}
			} else if (lineSplit[1].equalsIgnoreCase("dancing animals")) {
				if (AttractionsType.DANCING.contains(lineSplit[0].toLowerCase()) && findAttraction(lineSplit[1]) == false) {
					countValidLines += 1;
                    this.availableAttractions.add("dancing animals");
				}
			} else if (lineSplit[1].equalsIgnoreCase("counting masters")) {
				if (AttractionsType.COUNTING_MASTER.contains(lineSplit[0].toLowerCase()) && findAttraction(lineSplit[1]) == false) {
					countValidLines += 1;
                    this.availableAttractions.add("counting masters");
				}
			} else if (lineSplit[1].equalsIgnoreCase("passionate coders")) {
				if (AttractionsType.PASSIONATE_CODER.contains(lineSplit[0].toLowerCase()) && findAttraction(lineSplit[1]) == false) {
					countValidLines += 1;
                    this.availableAttractions.add("passionate coders");
				}
			}
		}

        this.resetAvailableAttractions();
		return countValidLines;
	}

	@Override
	public long countInvalidRecords() {
        long validLines = countValidRecords();
        long invalidLines = AttractionsType.AVAILABLE_ATTRACTIONS.length - validLines;
        return invalidLines;
	}

    private boolean findAttraction(String attraction) {
        for (String attract : this.availableAttractions) {
            if (attraction.equalsIgnoreCase(attract)) {
                return true;
            }
        }
        return false;
    }

    private void resetAvailableAttractions() {
        for (int i = 0; i < this.availableAttractions.size() + 3; i++) {
            this.availableAttractions.remove(0);
        }
    }
}

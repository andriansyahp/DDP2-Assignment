package javari.reader;

public interface AttractionsType {
    String[] AVAILABLE_ATTRACTIONS = {"Circles of Fire", "Dancing Animals", "Counting Masters", "Passionate Coders"};
	String CIRCLES_OF_FIRE = "lion, whale, eagle";
	String DANCING = "parrot, snake, cat, hamster";
	String COUNTING_MASTER = "hamster, whale, parrot";
	String PASSIONATE_CODER = "hamster, cat, snake";
}

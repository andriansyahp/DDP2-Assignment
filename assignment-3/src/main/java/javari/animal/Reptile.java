package javari.animal;

public class Reptile extends Animal {
	public static final String REPTILIAN_TYPE = "snake";
    private boolean isTame;
    /**
     * Constructs an instance of {@code Animal}.
     *
     * @param id        unique identifier
     * @param type      type of animal, e.g. Hamster, Cat, Lion, Parrot
     * @param name      name of animal, e.g. hamtaro, simba
     * @param gender    gender of animal (male/female)
     * @param length    length of animal in centimeters
     * @param weight    weight of animal in kilograms
     * @param condition health condition of the animal
     */
    public Reptile(Integer id, String type, String name, Gender gender, double length,
                  double weight, boolean isTame, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.isTame = isTame;
    }

    public boolean specificCondition() {
        return this.isTame;
    }
}

package javari.animal;

public class Aves extends Animal {
	public static final String AVES_TYPE = "parrot, eagle";
    private boolean isLayingEggs;
    
    /**
     * Constructs an instance of {@code Animal}.
     *
     * @param id        unique identifier
     * @param type      type of animal, e.g. Hamster, Cat, Lion, Parrot
     * @param name      name of animal, e.g. hamtaro, simba
     * @param gender    gender of animal (male/female)
     * @param length    length of animal in centimeters
     * @param weight    weight of animal in kilograms
     * @param condition health condition of the animal
     */
    public Aves(Integer id, String type, String name, Gender gender, double length,
                  double weight, boolean isLayingEggs, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.isLayingEggs = isLayingEggs;
    }

    public boolean specificCondition() {
        return this.isLayingEggs;
    }
}

package javari.animal;

public class Mammal extends Animal {
	public static final String MAMMALS_TYPE = "lion, whale, hamster, cat";	
    private boolean isPregnant;
    /**
     * Constructs an instance of {@code Animal}.
     *
     * @param id        unique identifier
     * @param type      type of animal, e.g. Hamster, Cat, Lion, Parrot
     * @param name      name of animal, e.g. hamtaro, simba
     * @param gender    gender of animal (male/female)
     * @param length    length of animal in centimeters
     * @param weight    weight of animal in kilograms
     * @param condition health condition of the animal
     */
    public Mammal(Integer id, String type, String name, Gender gender, double length,
                  double weight, boolean isPregnant, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        this.isPregnant = isPregnant;
    }

    public boolean specificCondition() {
        if (super.getType().equalsIgnoreCase("lion")) {
            if (super.getGender() == Gender.MALE) {
                return true;
            }
            return false;
        }
        return !this.isPregnant;
    }
}

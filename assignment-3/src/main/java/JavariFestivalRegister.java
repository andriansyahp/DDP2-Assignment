import javari.animal.*;
import javari.park.*;
import javari.reader.*;
import javari.writer.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.io.IOException;
import java.util.Scanner;


public class JavariFestivalRegister{
	static Scanner in = new Scanner(System.in);
	static final String ORIGINAL_INPUT_PATH = "C:\\Users\\Andriansyah Putra\\Documents\\Universitas Indonesia\\Semester 2\\DDP 2\\Lab\\DDP2-Assignment\\assignment-3\\data";
	static final String ORIGINAL_OUTPUT_PATH = "C:\\Users\\Andriansyah Putra\\Documents\\Universitas Indonesia\\Semester 2\\DDP 2\\Lab\\DDP2-Assignment\\assignment-3\\data";
	static final String[] CSV_FILE_NAMES = {"sections", "attractions", "categories", "records"};
	static Path[] paths;
	static CsvReader[] csvReaders;

	public static void main(String[] args) {
		paths = new Path[3];
		csvReaders = new CsvReader[4];

		System.out.println("Welcome to Javari Park Festival - Registration Service!\n");
		System.out.println("... Opening default section database from data. ");
		String inDirectory = ORIGINAL_INPUT_PATH;

		while (true) {
			try {
				for (int i = 1; i < CSV_FILE_NAMES.length; i++) {
					String fileName = inDirectory + "\\animals_" + CSV_FILE_NAMES[i];
					paths[i-1] = Paths.get(fileName + ".csv");
					if (i == 1) {
						csvReaders[1] = new AttractionCsvReader(paths[0]);
					} else if (i == 2) {
						csvReaders[2] = new CategoryCsvReader(paths[1]);
					} else if (i == 3) {
						csvReaders[3] = new RecordCsvReader(paths[2]);
					}
				}
				csvReaders[0] = new SectionCsvReader(paths[1]);
				break;
			} catch (IOException e) {
				System.out.println("... File not found or incorrect file!\n");
				System.out.print("Please provide the source data path: ");
				String inPath = in.nextLine();
				inDirectory = inPath;
			}
		}

		System.out.println("... Loading... Success... System is populating data...\n");
		System.out.printf("Found %d valid sections and %d invalid sections\n",
							csvReaders[0].countValidRecords(), csvReaders[0].countInvalidRecords());
		System.out.printf("Found %d valid attractions and %d invalid attractions\n",
							csvReaders[1].countValidRecords(), csvReaders[1].countInvalidRecords());
		System.out.printf("Found %d valid animal categories and %d invalid animal categories\n",
							csvReaders[2].countValidRecords(), csvReaders[2].countInvalidRecords());
		System.out.printf("Found %d valid animal records and %d invalid animal records\n\n",
							csvReaders[3].countValidRecords(), csvReaders[3].countInvalidRecords());

		lobby();
	}

	public static void lobby() {
		System.out.println("Welcome to Javari Park Festival - Registration Service!\n");
		System.out.println("Please answer the questions by typing the number.");
		System.out.println("Type # if you want to return to the previous menu");
		System.out.println("Javari Park has 3 sections:");
		System.out.println("1. Explore the Mammals\n2. World of Aves\n3. Reptilian Kingdom");
		System.out.print("Please choose your preferred section (type the number): ");

		String input = in.nextLine();
		if (input.equals("1")) {
			exploreMammals();
		} else if (input.equals("2")) {
			avesWorld();
		} else if (input.equals("3")) {
			reptileKingdom();
		} else if (input.equals("#")) {
			lobby();
		} else {
			System.out.println("Wrong input. Try Again!");
			lobby();
		}
	}

	public static void exploreMammals() {
		System.out.println("\n--Explore the Mammals--");
		System.out.println("1. Hamster\n2. Lion\n3. Cat\n4. Whale");
		System.out.print("Please choose your preferred animals (type the number): ");

		String input = in.nextLine();
		if (input.equals("1")) {
			if (findAnimalType("hamster")) {
				exploreHamster();
			} else {
				System.out.printf("Unfortunately, no hamster can perform any attraction, please choose other animals");
				exploreMammals();
			}
		} else if (input.equals("2")) {
			if (findAnimalType("lion")) {
				exploreLion();
			} else {
				System.out.printf("Unfortunately, no lion can perform any attraction, please choose other animals");
				exploreMammals();
			}
		} else if (input.equals("3")) {
			if (findAnimalType("cat")) {
				exploreCat();
			} else {
				System.out.printf("Unfortunately, no cat can perform any attraction, please choose other animals");
				exploreMammals();
			}
		} else if (input.equals("4")) {
			if (findAnimalType("whale")) {
				exploreWhale();
			} else {
				System.out.printf("Unfortunately, no whale can perform any attraction, please choose other animals");
				exploreMammals();
			}
		} else if (input.equals("#")) {
			lobby();
		} else {
			System.out.println("Wrong input. Try Again!");
			exploreMammals();
		}
	}

	public static void exploreHamster() {
		System.out.println("\n--Hamster--");
		System.out.println("Attractions by Hamster:");
		System.out.println("1. Counting Masters\n2. Passionate Coders");
		System.out.print("Please choose your preferred attractions (type the number): ");

		String input = in.nextLine();
		if (input.equals("1")) {
			registrationStep("Hamster", "Counting Masters");
		} else if (input.equals("2")) {
			registrationStep("Hamster", "Passionate Coders");
		} else if (input.equals("#")) {
			exploreMammals();
		} else {
			System.out.println("Wrong input. Try Again!");
			exploreHamster();
		}
	}

	public static void exploreLion() {
		System.out.println("\n--Lion--");
		System.out.println("Attractions by Lion:");
		System.out.println("1. Circles of Fire");
		System.out.print("Please choose your preferred attractions (type the number): ");

		String input = in.nextLine();
		if (input.equals("1")) {
			registrationStep("Lion", "Circles of Fire");
		} else if (input.equals("#")) {
			exploreMammals();
		} else {
			System.out.println("Wrong input. Try Again!");
			exploreLion();
		}
	}

	public static void exploreCat() {
		System.out.println("\n--Cat--");
		System.out.println("Attractions by Cat:");
		System.out.println("1. Dancing Animals\n2. Passionate Coders");
		System.out.print("Please choose your preferred attractions (type the number): ");

		String input = in.nextLine();
		if (input.equals("1")) {
			registrationStep("Cat", "Dancing Animals");
		} else if (input.equals("2")) {
			registrationStep("Cat", "Passionate Coders");
		} else if (input.equals("#")) {
			exploreMammals();
		} else {
			System.out.println("Wrong input. Try Again!");
			exploreCat();
		}
	}

	public static void exploreWhale() {
		System.out.println("\n--Whale--");
		System.out.println("Attractions by Whale:");
		System.out.println("1. Circles of Fire\n2. Counting Masters");
		System.out.print("Please choose your preferred attractions (type the number): ");

		String input = in.nextLine();
		if (input.equals("1")) {
			registrationStep("Whale", "Circles of Fire");
		} else if (input.equals("2")) {
			registrationStep("Whale", "Counting Masters");
		} else if (input.equals("#")) {
			exploreMammals();
		} else {
			System.out.println("Wrong input. Try Again!");
			exploreWhale();
		}
	}

	public static void avesWorld() {
		System.out.println("\n--World of Aves--");
		System.out.println("1. Parrot\n2. Eagle");
		System.out.print("Please choose your preferred animals (type the number): ");

		String input = in.nextLine();
		if (input.equals("1")) {
			if (findAnimalType("parrot")) {
				parrotWorld();
			} else {
				System.out.printf("Unfortunately, no parrot can perform any attraction, please choose other animals");
				avesWorld();
			}
		} else if (input.equals("2")) {
			if (findAnimalType("eagle")) {
				eagleWorld();
			} else {
				System.out.printf("Unfortunately, no eagle can perform any attraction, please choose other animals");
				avesWorld();
			}
		} else if (input.equals("#")) {
			lobby();
		} else {
			System.out.println("Wrong input. Try Again!");
			avesWorld();
		}
	}

	public static void parrotWorld() {
		System.out.println("\n--Parrot--");
		System.out.println("Attractions by Parrot:");
		System.out.println("1. Dancing Animals\n2. Counting Masters");
		System.out.print("Please choose your preferred attractions (type the number): ");

		String input = in.nextLine();
		if (input.equals("1")) {
			registrationStep("Parrot", "Dancing Animals");
		} else if (input.equals("2")) {
			registrationStep("Parrot", "Counting Masters");
		} else if (input.equals("#")) {
			avesWorld();
		} else {
			System.out.println("Wrong input. Try Again!");
			parrotWorld();
		}
	}

	public static void eagleWorld() {
		System.out.println("\n--Eagle--");
		System.out.println("Attractions by Eagle:");
		System.out.println("1. Circles of Fire");
		System.out.print("Please choose your preferred attractions (type the number): ");

		String input = in.nextLine();
		if (input.equals("1")) {
			registrationStep("Eagle", "Circles of Fire");
		} else if (input.equals("#")) {
			exploreMammals();
		} else {
			System.out.println("Wrong input. Try Again!");
			eagleWorld();
		}
	}

	public static void reptileKingdom() {
		System.out.println("\n--Reptilian Kingdom--");
		System.out.println("1. Snake");
		System.out.print("Please choose your preferred animals (type the number): ");

		String input = in.nextLine();
		if (input.equals("1")) {
			if (findAnimalType("snake")) {
				snakeKingdom();
			} else {
				System.out.printf("Unfortunately, no snake can perform any attraction, please choose other animals");
				reptileKingdom();
			}
		} else if (input.equals("#")) {
			lobby();
		} else {
			System.out.println("Wrong input. Try Again!");
			avesWorld();
		}
	}

	public static void snakeKingdom() {
		System.out.println("\n--Snake--");
		System.out.println("Attractions by Snake:");
		System.out.println("1. Dancing Animals\n2. Passionate Coders");
		System.out.print("Please choose your preferred attractions (type the number): ");

		String input = in.nextLine();
		if (input.equals("1")) {
			registrationStep("Snake", "Dancing Animals");
		} else if (input.equals("2")) {
			registrationStep("Snake", "Passionate Coders");
		} else if (input.equals("#")) {
			reptileKingdom();
		} else {
			System.out.println("Wrong input. Try Again!");
			snakeKingdom();
		}
	}

	public static void registrationStep(String animalType, String attraction) {
		System.out.println("Wow, one more step,\n");
		System.out.print("please let us know your name: ");
		String input = in.nextLine();
		Regist regis = new Regist(input);
		System.out.println("Name: " + input);
		System.out.println("Attractions: " + attraction + " -> " + animalType);

		System.out.print("\nIs the data correct? (Y/N): ");
		if (in.nextLine().equalsIgnoreCase("n")) {
			lobby();
		}

		System.out.print("\nThank you for your interest. Would you like to register to other attractions? (Y/N): ");
		if (in.nextLine().equalsIgnoreCase("y")) {
			lobby();
		}

		//jsonWrite(regis);
	}

	/*public static void jsonWrite(Regist regist) {
		String outDirectory = ORIGINAL_OUTPUT_PATH;

		while (true) {
			try {
				RegistrationWriter.writeJson(regist, Paths.get(outDirectory));
				System.out.println("End of program");
				break;
			} catch (IOException e) {
				System.out.println("Please provide the output directory path: ");
				outDirectory = in.nextLine();
			}
		}
    }*/

	private static boolean findAnimalType(String type) {
		for (Animal animal : ((RecordCsvReader) csvReaders[3]).getAnimals()) {
			if (type.equalsIgnoreCase(animal.getType())) {
				return true;
			}
		}
		return false;
	}
}

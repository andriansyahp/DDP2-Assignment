public class TrainCar {

    public static final double EMPTY_WEIGHT = 20; // In kilograms

    WildCat cat;
    TrainCar next;

    // Initiating constructors, overloaded
    public TrainCar(WildCat cat) {
        this.cat = cat;
    }

    public TrainCar(WildCat cat, TrainCar next) {
        this.cat = cat;
        this.next = next;
    }

    // Make a method to compute total weight of the train which contains cats inside the cars plus
    // the weight of all the cars of the train recursively
    public double computeTotalWeight() {
        // Branching selection
        if (this.next == null) {
            // Return default weight of the car plus the weight of the cat inside the current
            // car if the car is the last car of the train
            return EMPTY_WEIGHT + this.cat.weight;
        }
        // Return default weight of the car plus the weight of the cat inside the current
        // car plus the weight of the next cars of the train if the current car is not the last
        // car of the train
        return EMPTY_WEIGHT + this.cat.weight + this.next.computeTotalWeight();
    }

    // Make a method to compute total Body Mass Index of the cats inside the train
    // that make use of the computeMassIndex() method in WildCat class recursively
    public double computeTotalMassIndex() {
        // Branching selection
        if (this.next == null) {
            // Return the BMI of the cat inside the current car if the cat is in the
            // last car of the train
            return this.cat.computeMassIndex();
        }
        // Return the BMI of the cat inside the current car  plus the BMI of the cat
        // in the next cars of the train if the cat is not in the last car of the train
        return this.cat.computeMassIndex() + this.next.computeTotalMassIndex();
    }

    // Make a method to print all the cats inside the train recursively
    public void printCar() {
        // Branching selection
        if (this.next == null) {
            // Print the cat inside the current car if the car is the last car of the train
            System.out.printf(String.format("--(%s)\n", this.cat.name));
        } else {
            // Print the cat inside the current car followed by printing the cats inside
            // the next cars of the train if the car is not the last car of the train
            System.out.printf("--(%s)", this.cat.name);
            this.next.printCar();
        }
    }
}

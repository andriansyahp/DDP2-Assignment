import java.util.Scanner;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms

    // Initiate static variables of the cats and the train
    static WildCat catInit;
    static TrainCar trainInit;

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        // Initiate variable to store an initial state of the station
        // to check if there is a train to pick up the cats or not
        boolean hasTrain = false;
        // Initiate variable to be the counter of the number of cats in the train
        int catsInTrain = 0;

        // Printing the user interface to ask for the number of cats to be transported
        System.out.print("Number of cat(s) to be transported: ");
        int numOfCat = Integer.parseInt(input.nextLine());
        for (int i = 0; i < numOfCat; i++) {
            System.out.print(">>> ");
            String[] cat = input.nextLine().split(",");
            // Instantiate a new cat that takes parameters of the given input
            catInit = new WildCat(cat[0], Double.parseDouble(cat[1]), Double.parseDouble(cat[2]));

            // Branching selection check if there is a train already or not
            if (hasTrain == false) {
                // Instantiate a new, first car to form the train
                trainInit = new TrainCar(catInit);
                // Since there is already a train to pick up the cats, the hasTrain value has
                // changed to true
                hasTrain = true;
                // Add up the cats counter by 1 cat each instantiation
                catsInTrain += 1;
            } else {
                // Instantiate a new car to form the train with the previous car
                trainInit = new TrainCar(catInit, trainInit);
                // Add up the cats counter by 1 cat each instantiation
                catsInTrain += 1;
            }

            // Make a condition to depart the train, which is if the total weight of the train
            // is more than the treshold or the end of the input has been reached
            if (trainInit.computeTotalWeight() > THRESHOLD || i == numOfCat - 1) {
                trainDepart(trainInit, catsInTrain);
                // Reset the cats counter to zero
                catsInTrain = 0;
                // Reset the condition of the station to don't has a train
                hasTrain = false;
            }
        }
    }

    // Make a method to depart the train from the station
    private static void trainDepart(TrainCar train, int catsInTrain) {
        // Print the interface of the program, showing each cars of the train that departed from
        // the station
        System.out.print("The train departs to Javari Park\n[LOCO]<");
        train.printCar();
        // Initiate a variable to store the value of average BMI of the cats in the train
        double avgMass = train.computeTotalMassIndex() / catsInTrain;
        // Printing the average BMI rounded to 2 decimal places and it's BMI category
        System.out.printf("Average mass index of all cats: %.2f\n", avgMass);
        System.out.printf("In average, the cats in the train are *%s*\n", categoryCheck(avgMass));
    }

    // Make a method to check the category of the cat's BMI
    private static String categoryCheck(double avgMassIndex) {
        if (avgMassIndex < 18.5) {
            return "underweight";
        } else if (avgMassIndex >= 18.5 && avgMassIndex < 25) {
            return "normal";
        } else if (avgMassIndex >= 25 && avgMassIndex < 30) {
            return "overweight";
        }
        return "obese";
    }
}

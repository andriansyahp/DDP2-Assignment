public class WildCat {

    String name;
    double weight; // In kilograms
    double length; // In centimeters

    // Initiating constructor
    public WildCat(String name, double weight, double length) {
        this.name = name;
        this.weight = weight;
        this.length = length;
    }

    // Make a method to compute Body Mass Index (BMI) of the cat based on BMI Formula
    public double computeMassIndex() {
        double catBodyMassIndex = (this.weight * 10000) / (this.length * this.length);
        return catBodyMassIndex;
    }
}
